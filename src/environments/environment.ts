// This file can be replaced during build by using the `fileReplacements` array.
// `ng build --prod` replaces `environment.ts` with `environment.prod.ts`.
// The list of file replacements can be found in `angular.json`.

export const environment = {
  production: false,
  firebase:{
    apiKey: "AIzaSyANGzJ0QASvRHMvTINQEeq-VkFo8lK-t2E",
    authDomain: "firestore-5a641.firebaseapp.com",
    databaseURL: "https://firestore-5a641.firebaseio.com",
    projectId: "firestore-5a641",
    storageBucket: "firestore-5a641.appspot.com",
    messagingSenderId: "857978641954",
    appId: "1:857978641954:web:b193e31e53ebf8d0d7b47b",
    measurementId: "G-4RLWNEGVJR"  
  }
};

/*
 * For easier debugging in development mode, you can import the following file
 * to ignore zone related error stack frames such as `zone.run`, `zoneDelegate.invokeTask`.
 *
 * This import should be commented out in production mode because it will have a negative impact
 * on performance if an error is thrown.
 */
// import 'zone.js/dist/zone-error';  // Included with Angular CLI.
