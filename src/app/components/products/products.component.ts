import { Component, OnInit } from '@angular/core';
import {ProductService} from '../../services/product.service';
import { Product } from 'src/app/models/product';

@Component({
  selector: 'app-products',
  templateUrl: './products.component.html',
  styleUrls: ['./products.component.scss']
})
export class ProductsComponent implements OnInit {
  products=[]
  editingProduct: Product;
  editing:Boolean=false;

  constructor(public productService:ProductService) { }

  ngOnInit(): void {
    this.productService.getProducts().subscribe(products=>{

      this.products=products;
      console.log(products);
    })
  }

  DeleteProduct(event,product){
    this.productService.deleteProducts(product);
    console.log(product);
    
  }

  EditProduct(event,product){
    this.editingProduct=product;
    this.editing=!this.editing;
    
  }


  UpdateProduct(event,product){
    this.productService.updateProducts(this.editingProduct);
    this.editingProduct={ } as Product;
    this.editing=false;

  }

}
