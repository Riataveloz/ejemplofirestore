import { Injectable } from '@angular/core';
import {AngularFirestore, AngularFirestoreCollection, AngularFirestoreDocument} from '@angular/fire/firestore';
import{Observable} from 'rxjs'
import {Product} from '../models/product'
import {map} from 'rxjs/operators';
import { ActionSequence } from 'protractor';


@Injectable({
  providedIn: 'root'
})
export class ProductService {

  products:Observable<Product[]>;
  productsCollection:AngularFirestoreCollection<Product>;
  productDoc:AngularFirestoreDocument<Product>;

  constructor(public db:AngularFirestore) {
    //this.products=this.db.collection('items').valueChanges();
    this.productsCollection=this.db.collection('items');
    this.products=this.productsCollection.snapshotChanges().pipe(map(actions=>{
      return actions.map(a=>{
        const data= a.payload.doc.data() as Product;
        data.id=a.payload.doc.id;
        return data;
      });
    }));
   }

   addProduct(product:Product){
     this.productsCollection.add(product);
   }


   getProducts(){
     return this.products;
   }

   deleteProducts(product:Product){
     this.productDoc=this.db.doc(`items/${product.id}`);
     this.productDoc.delete();
   }

   updateProducts(product:Product){
    this.productDoc=this.db.doc(`items/${product.id}`);
    this.productDoc.update(product);
  }

  editProducts(product:Product){
    this.productDoc=this.db.doc(`items/${product.id}`);
    this.productDoc.delete();
  }

}
